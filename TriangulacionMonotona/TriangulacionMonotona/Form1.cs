﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ParticionMonotona
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Point> vertices = new List<Point>();
        private bool band = true;

        private ParticionMonotona miParticionMonotona = new ParticionMonotona();

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (band)
            {
                using (Graphics grafico = CreateGraphics())
                    grafico.FillEllipse(new SolidBrush(Color.Black), e.X - 2, e.Y - 2, 5, 5);
                miParticionMonotona.AgregarVertices(e.X, e.Y);
                AgregarPunto(e.X, e.Y);
                DibujarPoligono();
            }
        }

        public void DibujarPoligono()
        {
            if (vertices.Count > 1)
            {
                Graphics graphics = CreateGraphics();

                for (int i = 0; i < vertices.Count - 1; i++)
                {
                    graphics.DrawLines(new Pen(Color.Black), vertices.ToArray());
                }
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            xLabel.Text = "X: " + e.X;
            yLabel.Text = "Y: " + e.Y;
        }

        private void AgregarPunto(int x, int y)
        {
            Point p = new Point(x, y);
            vertices.Add(p);
        }

        private void toolStripButton1_Click(object sender, System.EventArgs e)
        {
            if (band)
            {
                using (Graphics grafico = CreateGraphics())
                {
                    grafico.FillPolygon(new SolidBrush(Color.White), vertices.ToArray());
                    grafico.DrawPolygon(new Pen(Color.Blue), vertices.ToArray());

                    for (int i = 0; i < vertices.Count; i++)
                        grafico.FillEllipse(new SolidBrush(Color.Black), vertices[i].X - 2, vertices[i].Y - 2, 5, 5);
                }

                band = false;

                if (!miParticionMonotona.HacerMonotono(this))
                    Limpiar();
            }
        }

        private void newToolStripButton_Click(object sender, System.EventArgs e)
        {
            Limpiar();
        }

        private void Limpiar()
        {
            miParticionMonotona.Vaciar();
            vertices.Clear();
            using (Graphics grafico = CreateGraphics())
                grafico.Clear(Color.FromArgb(240, 240, 240));
            band = true;
        }

        private void helpToolStripButton_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Solo tienes que dar clicks dibujar el poligono y luego presionar el segundo boton o presionar Enter para particionar el poligono", "Ayuda", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }

        private void toolStripButton2_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Este es un programa que particiona un poligono en piezas y-monotonas. Desarrollado por Hans Pierre Blanco, estudiante de ingeniería en computación, UNI-Nicaragua", "Acerca de...", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && band)
            {
                if (band)
                {
                    using (Graphics grafico = CreateGraphics())
                    {
                        grafico.FillPolygon(new SolidBrush(Color.White), vertices.ToArray());
                        grafico.DrawPolygon(new Pen(Color.Blue), vertices.ToArray());

                        for (int i = 0; i < vertices.Count; i++)
                            grafico.FillEllipse(new SolidBrush(Color.Black), vertices[i].X - 2, vertices[i].Y - 2, 5, 5);
                    }

                    band = false;

                    if (!miParticionMonotona.HacerMonotono(this))
                        Limpiar();
                }
            }
        }
    }
}
