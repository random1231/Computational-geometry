﻿namespace ParticionMonotona
{
    public enum tiposDeVertices
    {
        START_VERTEX,
        END_VERTEX,
        REGULAR_LEFT_VERTEX,
        REGULAR_RIGHT_VERTEX,
        SPLIT_VERTEX,
        MERGE_VERTEX
    };
}
