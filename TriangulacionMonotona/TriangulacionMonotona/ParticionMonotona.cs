﻿using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;

namespace ParticionMonotona
{
    class ParticionMonotona
    {
        private List<Punto> verticesPoligono = new List<Punto>(); 
        private List<Arista> Aristas = new List<Arista>();
        private bool antihorario;        

        public void AgregarVertices(int x, int y)
        {
            Punto vertice = new Punto(x, y);
            verticesPoligono.Add(vertice);
        }

        public void Vaciar()
        {
            verticesPoligono.Clear();
            Aristas.Clear();
        }

        private void Asignar() //Asigna siguiente y anterior a cada punto
        {
            if (AreaPoligono() < 0)
                antihorario = true;
            else
                antihorario = false;

            for (int i = 0; i < verticesPoligono.Count; i++)
            {
                if (antihorario)
                {
                    if (i == 0)
                    {
                        verticesPoligono[i].Anterior = verticesPoligono[verticesPoligono.Count - 1];
                        verticesPoligono[i].Siguiente = verticesPoligono[i + 1];
                    }

                    else if (i == verticesPoligono.Count - 1)
                    {
                        verticesPoligono[i].Anterior = verticesPoligono[i - 1];
                        verticesPoligono[i].Siguiente = verticesPoligono[0];
                    }

                    else
                    {
                        verticesPoligono[i].Anterior = verticesPoligono[i - 1];
                        verticesPoligono[i].Siguiente = verticesPoligono[i + 1];
                    }
                }

                else
                {
                    if (i == 0)
                    {
                        verticesPoligono[i].Siguiente = verticesPoligono[verticesPoligono.Count - 1];
                        verticesPoligono[i].Anterior = verticesPoligono[i + 1];
                    }

                    else if (i == verticesPoligono.Count - 1)
                    {
                        verticesPoligono[i].Siguiente = verticesPoligono[i - 1];
                        verticesPoligono[i].Anterior = verticesPoligono[0];
                    }

                    else
                    {
                        verticesPoligono[i].Siguiente = verticesPoligono[i - 1];
                        verticesPoligono[i].Anterior = verticesPoligono[i + 1];
                    }
                }
            }
        }

        private int Area(Punto p1, Punto p2, Punto p3)
        {
            return (p1.X * p2.Y) + (p2.X * p3.Y) + (p3.X * p1.Y) -
                      (p1.X * p3.Y) - (p2.X * p1.Y) - (p3.X * p2.Y);       
        }

        private double AreaPoligono()
        {
            double area = 0;
            for (int i = 1; i < verticesPoligono.Count - 2; i++)
                area += Area(verticesPoligono[0], verticesPoligono[i], verticesPoligono[i + 1]);            
            return area;
        }

        private int Orientacion(Punto p)
        {
            int res = Area(p.Anterior, p, p.Siguiente);

            if (res == 0)
                return 0; //Colinear
            return (res > 0) ? 1 : -1; //Retorna 1 si el area es positiva y -1 si es negativa  
        }

        private bool InterseccionDeSegmentos(Punto p1, Punto p2,
                              Punto p3, Punto p4, ref PointF i)
        {
            float mayor1;
            float menor1;
            float mayor2;
            float menor2;
            float A1 = p2.Y - p1.Y;
            float B1 = p1.X - p2.X;
            float C1 = A1 * p1.X + B1 * p1.Y;
            float A2 = p4.Y - p3.Y;
            float B2 = p3.X - p4.X;
            float C2 = A2 * p3.X + B2 * p3.Y;
            float denom = A1 * B2 - A2 * B1;

            if (denom == 0.0)
                return false;
            i.X = (C1 * B2 - C2 * B1) / denom;
            i.Y = (A1 * C2 - A2 * C1) / denom;

            if (p1.X > p2.X)
            {
                mayor1 = p1.X;
                menor1 = p2.X;

            }

            else
            {
                mayor1 = p2.X;
                menor1 = p1.X;
            }

            if (p3.X > p4.X)
            {
                mayor2 = p3.X;
                menor2 = p4.X;

            }
            else
            {
                mayor2 = p4.X;
                menor2 = p3.X;
            }

            if (i.X >= menor1 && i.X <= mayor1 &&
                i.X >= menor2 && i.X <= mayor2)
                return true;
            return false;
        }

        private int tipoVertice(Punto p)
        {
            int orientacion = Orientacion(p);
            int tdv = -1; //tipo de vertice

            if (p.Y <= p.Anterior.Y && p.Y >= p.Siguiente.Y)
                tdv = (int)tiposDeVertices.REGULAR_RIGHT_VERTEX;
            else if (p.Y >= p.Anterior.Y && p.Y <= p.Siguiente.Y)
                tdv = (int)tiposDeVertices.REGULAR_LEFT_VERTEX;            

            else if (orientacion == -1)
            {
                if (p.Y < p.Anterior.Y && p.Y < p.Siguiente.Y)
                    tdv = (int)tiposDeVertices.START_VERTEX;
                else if (p.Y > p.Anterior.Y && p.Y > p.Siguiente.Y)
                    tdv = (int)tiposDeVertices.END_VERTEX;
            }

            else if (orientacion == 1)
            {
                if (p.Y < p.Anterior.Y && p.Y < p.Siguiente.Y)
                    tdv = (int)tiposDeVertices.SPLIT_VERTEX;
                else if (p.Y > p.Anterior.Y && p.Y > p.Siguiente.Y)
                    tdv = (int)tiposDeVertices.MERGE_VERTEX;
            }
            return tdv;
        }

        private void AsignarTipoDeVertice()
        {            
            for (int i = 0; i < verticesPoligono.Count; ++i)
                verticesPoligono[i].TipoDeVertice = tipoVertice(verticesPoligono[i]);
        }

        private bool EsMonotono()
        {
            for (int i = 0; i < verticesPoligono.Count; ++i)
                if (verticesPoligono[i].TipoDeVertice == (int)tiposDeVertices.SPLIT_VERTEX ||
                    verticesPoligono[i].TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
                    return false;
            return true;
        }

        public Arista LadoALaIzquierda(Punto vertice)
        {
            Punto p = new Punto(0, vertice.Y);
            PointF x = new PointF();
            Arista a = new Arista(null, null);
            float mayor = -1;
            
            for (int i = 0; i < Aristas.Count; i++)
            {
                if (InterseccionDeSegmentos(vertice, p, Aristas[i].Lado, 
                    Aristas[i].Lado.Siguiente, ref x))
                {
                    if (mayor == -1)
                    {
                        mayor = x.X;
                        a = Aristas[i];
                    }

                    else
                    {
                        if (x.X > mayor)
                        {
                            mayor = x.X;
                            a = Aristas[i];
                        }
                    }
                }
            }

            return a;
        }

        private bool EsPoligonoSimple()
        {
            int intercepciones = 0;
            Punto p = verticesPoligono[0];
            Punto q = p.Siguiente;

            PointF inte = new PointF();

            for (int i = 0; i < verticesPoligono.Count; i++)
            {
                intercepciones = 0;
                for (int j = 0; j < verticesPoligono.Count - 1; j++)
                {
                    if (InterseccionDeSegmentos(p, p.Siguiente, q, q.Siguiente, ref inte))
                        intercepciones++;
                    q = q.Siguiente;
                }

                if (intercepciones > 2)
                    return false;
                p = p.Siguiente;
            }
            return true;
        }

        private void ManejarStartVertex(Punto startVertex)
        {
            startVertex.Lado = new Arista(startVertex, startVertex, Aristas.Count);
            Aristas.Add(startVertex.Lado);
        }

        private void manejarEndVertex(Punto endVertex, Form form)
        {
            if (endVertex.Anterior.Lado.Helper.TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
            {
                Graphics grafico = form.CreateGraphics();
                grafico.DrawLine(new Pen(Color.Black), endVertex.X, endVertex.Y,
                    endVertex.Anterior.Lado.Helper.X, endVertex.Anterior.Lado.Helper.Y);                
            }

            Aristas.Remove(endVertex.Anterior.Lado);
        }

        private void ManejarMergeVertex(Punto mergeVertex, Form form)
        {
            if (mergeVertex.Anterior.Lado.Helper.TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
            {
                Graphics grafico = form.CreateGraphics();
                grafico.DrawLine(new Pen(Color.Black), mergeVertex.X, mergeVertex.Y,
                    mergeVertex.Anterior.Lado.Helper.X, mergeVertex.Anterior.Lado.Helper.Y);
            }

            Aristas.Remove(mergeVertex.Anterior.Lado);

            Arista a = LadoALaIzquierda(mergeVertex);           

            if (a.Helper.TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
            {
                Graphics grafico = form.CreateGraphics();
                grafico.DrawLine(new Pen(Color.Black), mergeVertex.X, mergeVertex.Y, a.Helper.X, a.Helper.Y);
            }

            a.Helper = mergeVertex;            
        }
    
        private void manejarSplitVertex(Punto splitVertex, Form form)
        {
            Arista a = LadoALaIzquierda(splitVertex);
            Graphics grafico = form.CreateGraphics();

            grafico.DrawLine(new Pen(Color.Black), splitVertex.X, splitVertex.Y, a.Helper.X, a.Helper.Y);
            a.Helper = splitVertex;
            splitVertex.Lado = new Arista(splitVertex, splitVertex, Aristas.Count + 1);
            Aristas.Add(splitVertex.Lado);
        }

        private void manejarRegularRightVertex(Punto regularRightVertex, Form form)
        {
            Arista a = LadoALaIzquierda(regularRightVertex);            

            if (a.Helper.TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
            {
                Graphics grafico = form.CreateGraphics();
                grafico.DrawLine(new Pen(Color.Black), regularRightVertex.X, regularRightVertex.Y, a.Helper.X, a.Helper.Y);
            }

            a.Helper = regularRightVertex;
        }

        private void manejarRegularLeftVertex(Punto regularLeftVertex, Form form)
        {
            if (regularLeftVertex.Anterior.Lado.Helper.TipoDeVertice == (int)tiposDeVertices.MERGE_VERTEX)
            {
                Graphics grafico = form.CreateGraphics();
                grafico.DrawLine(new Pen(Color.Black), regularLeftVertex.X, regularLeftVertex.Y,
                    regularLeftVertex.Anterior.Lado.Helper.X, regularLeftVertex.Anterior.Lado.Helper.Y);
            }

            Aristas.Remove(regularLeftVertex.Anterior.Lado);

            regularLeftVertex.Lado = new Arista(regularLeftVertex, regularLeftVertex, Aristas.Count);
            Aristas.Add(regularLeftVertex.Lado);
        }

        public bool HacerMonotono(Form form)
        {
            bool error = false;
            Asignar();
            verticesPoligono = verticesPoligono.OrderBy(p => p.Y).ToList();
            AsignarTipoDeVertice();
            if (!EsPoligonoSimple())
            {
                MessageBox.Show("No es un poligono simple", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
                
            if (EsMonotono())
                MessageBox.Show("Es y-monotono");            
            
            for (int i = 0; i < verticesPoligono.Count; ++i)
            {
                try
                {
                    switch (verticesPoligono[i].TipoDeVertice)
                    {
                        case (int)tiposDeVertices.START_VERTEX:
                            ManejarStartVertex(verticesPoligono[i]);
                            break;
                        case (int)tiposDeVertices.REGULAR_LEFT_VERTEX:
                            manejarRegularLeftVertex(verticesPoligono[i], form);
                            break;
                        case (int)tiposDeVertices.REGULAR_RIGHT_VERTEX:
                            manejarRegularRightVertex(verticesPoligono[i], form);
                            break;
                        case (int)tiposDeVertices.END_VERTEX:
                            manejarEndVertex(verticesPoligono[i], form);
                            break;
                        case (int)tiposDeVertices.MERGE_VERTEX:
                            ManejarMergeVertex(verticesPoligono[i], form);
                            break;
                        case (int)tiposDeVertices.SPLIT_VERTEX:
                            manejarSplitVertex(verticesPoligono[i], form);
                            break;
                    }
                }

                catch (System.Exception)
                {
                    error = true;                    
                }
            }

            if (error)
            {
                MessageBox.Show("Lo siento, ocurrio algo inesperado" , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
 