﻿using System;
using System.Collections.Generic;

namespace ParticionMonotona
{
    class Arista
    {
        private Punto lado; //lado esta definida por un un punto p y el siguiente de p
        private Punto helper;
        private int indice;

        public Arista(Punto p, Punto ayudante)
        {
            Lado = p;
            Helper = ayudante;
        }

        public Arista(Punto p, Punto ayudante, int i)
        {
            Lado = p;
            Helper = ayudante;
            Indice = i;
        }

        public int Indice
        {
            set
            {
                indice = value;
            }

            get
            {
                return indice;
            }
        }

        public Punto Lado
        {
            set
            {
                lado = value;
            }

            get
            {
                return lado;
            }
        }

        public Punto Helper
        {
            set
            {
                helper = value;
            }

            get
            {
                return helper;
            }
        }    
    }
}
