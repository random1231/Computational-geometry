﻿using System.Drawing;

namespace ParticionMonotona
{
    class Punto
    {        
        private int tipoDeVertice;
        private Point vertice;
        private Arista lado;
        private Punto siguiente;
        private Punto anterior;

        public Punto(int X, int Y)
        {
            vertice = new Point(X, Y);
        }        

        public int X
        {
            set
            {
                vertice.X = value;
            }

            get
            {
                return vertice.X;
            }
        }

        public int Y
        {
            set
            {
                vertice.Y = value;
            }

            get
            {
                return vertice.Y;
            }
        }

        public int TipoDeVertice
        {
            set
            {
                tipoDeVertice = value;
            }

            get
            {
                return tipoDeVertice;
            }
        }

        public Arista Lado
        {
            set
            {
                lado = value;
            }

            get
            {
                return lado;
            }
        }

        public Punto Siguiente
        {
            set
            {
                siguiente = value;
            }

            get
            {
                return siguiente;
            }
        }

        public Punto Anterior
        {
            set
            {
                anterior = value;
            }

            get
            {
                return anterior;
            }
        }

    }
}
